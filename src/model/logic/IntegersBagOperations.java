package model.logic;

import java.util.Iterator;

import model.data_structures.IntegersBag;

public class IntegersBagOperations {

	
	
	public double computeMean(IntegersBag bag){
		double mean = 0;
		int length = 0;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				mean += iter.next();
				length++;
			}
			if( length > 0) mean = mean / length;
		}
		return mean;
	}
	
	
	public int getMax(IntegersBag bag){
		int max = Integer.MIN_VALUE;
	    int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( max < value){
					max = value;
				}
			}
			
		}
		return max;
	}
	
	public int getMin(IntegersBag bag){
		int min = Integer.MAX_VALUE;
		int value;
		if( bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( min < value){
					min = value;
				}
			}
			return min;
		}
		return min;
	}
	

	public int darTotal(IntegersBag bag){
		int suma = 0;
		int value;
		if( bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
	
				value = iter.next();
				
				suma+=value;
				
			}
			return suma;
		}
		return suma;
	}
	
	public int darPromedio(IntegersBag bag){
		int cont = 0;
		int total = darTotal(bag);
		if(total != 0){
			Iterator<Integer> iter = bag.getIterator();
			while (iter.hasNext()) {
				cont++;
			}
			return total/cont;
		}
		return 0;
	}
}
